# demeter

Server-side nodejs utilities.

For the full documentation, installation instructions... check [demeter's documentation page](https://squeak.eauchat.org/libs/demeter/).
