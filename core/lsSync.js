var fs = require("fs");
var _ = require("underscore");
var $$ = require("squeak");
require("squeak/extension/path");
var demeter = require("./_");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var defaultOptions = {
  serverPrefixPath: "",
  recursive: false,
  flat: false,
  includeDirectories: false,
};

/**
  DESCRIPTION:
    Return the list of folders and files in the given directory (recursively if asked).
    The return is an array of objects containing information about the given element.
  ARGUMENTS: (
    <string|{
      !browserPath: <string> « pass here path, relative to 'serverPrefixPath' »,
      !serverPrefixPath: <string>@default="" « pass here the part of the path that shouldn't be sent to the server »,
      ?recursive: <boolean>@default=false « if true, will also list children files recusively »,
      ?flat: <boolean>@default=false « if true, return a flat list of all files »,
      ?includeDirectories: <boolean>@default=false « if recursive and flat, true will include directories in list »,
    }> « passing just a string is the same as passing { browserPath: string } »
  )
  RETURN: <pathObject[]>
  TYPES:
    pathObject = {
      parentPath: <string> « /path/to/foo/ »,
      path: <string> « /path/to/foo/bar.ext »,
      name: <string> « bar »,
      extension: <string> « ext »,
      nameAndExtension: <string> « bar.ext »,
      directory: <boolean>,
      children: <pathObject[]|undefined>
    }
  EXAMPLES:
    demeter.lsSync({
      browserPath: "/path/to/directory/",
      serverPrefixPath: ".",
      recursive: true,
    });
    // > [
    // >   {
    // >     parentPath: "/path/to/directory/",
    // >     path: "/path/to/directory/file.ext",
    // >     name: "file",
    // >     extension: "ext",
    // >     nameAndExtension: "file.ext",
    // >     isDirectory: false,
    // >   },
    // >   {
    // >     parentPath: "/path/to/directory/",
    // >     path: "/path/to/directory/subdir/",
    // >     name: "subdir",
    // >     extension: "",
    // >     nameAndExtension: "subdir",
    // >     isDirectory: true,
    // >     children: [
    // >       {
    // >         parentPath: "/path/to/directory/subdir/",
    // >         path: "/path/to/directory/subdir/subfile.ext",
    // >         name: "subfile",
    // >         extension: "ext",
    // >         nameAndExtension: "subfile.ext",
    // >         isDirectory: false,
    // >       }
    // >     ],
    // >   },
    // > ]
*/
demeter.lsSync = function (options) {

  //
  //                              HANDLE JUST PASSING A PATH

  if (_.isString(options)) options = { browserPath: options };

  //
  //                              GET ALL OPTIONS AND DETERMINE PATH

  options = $$.defaults(defaultOptions, options);
  // make sure there is trailing slash at the end
  options.browserPath = options.browserPath.replace(/\/*$/, "/")

  var directoryPath = options.serverPrefixPath + options.browserPath;

  //
  //                              VERIFY DIRECTORY EXISTS, THAT IT'S A DIRECTORY

  // check that directory exists
  try {
    var directory = fs.statSync(directoryPath);
  }
  catch (err) {
    $$.log.detailedError(
      "demeter/ls",
      "Error directory you want to list doesn't exist.",
      directoryPath,
      err
    );
    return demeter.error(directoryPath +" doesn't exist", 404);
  };
  // check that it's a directory
  if (!directory.isDirectory()) return demeter.error(directoryPath +" is not a directory", 405);

  //
  //                              LS

  var results = [];

  fs.readdirSync(directoryPath).forEach(function (fileName) {

    // get file stats
    var filePath = directoryPath + fileName;
    var stat = fs.statSync(filePath);

    // get file infos
    var onlinePath = filePath.replace(new RegExp ("^"+ $$.regexp.escape(options.serverPrefixPath)), "");
    var decomposedPath = $$.path.decompose(onlinePath + (stat.isDirectory() ? "/" : ""));
    var fileInfos = {
      path: decomposedPath.path,
      parentPath: decomposedPath.parentPath,
      name: decomposedPath.name,
      extension: decomposedPath.extension,
      nameAndExtension: decomposedPath.nameAndExtension,
      isDirectory: stat.isDirectory(),
    };

    // PUSH FILES INFO TO LIST
    if (!stat.isDirectory() || !options.flat || options.includeDirectories) results.push(fileInfos);

    // DO FOR SUBDIRECTORIES IF ASKED
    if (options.recursive && stat && stat.isDirectory()) {
      var opts = _.clone(options);
      opts.browserPath += fileName +"/";
      if (options.flat) results.push(demeter.lsSync(opts));
      else fileInfos.children = demeter.lsSync(opts);
    };

  });

  //
  //                              RETURN LIST

  return _.flatten(results, true);

  //                              ¬
  //

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = demeter.lsSync;
