# Usage

Load and use demeter library with:

```javascript
const demeter = require("demeter");
let lsResult = demeter.lsSync(options);
```

If you don't want to import the whole library. You can import single methods with the following pattern:
`require(demeter/<group>/<method>)`
For example:
```javascript
const demeterLsSync = require("demeter/core/lsSync");
let lsResult = demeterLsSync(options);
```

### Extensions and plugins are not imported by default when you import demeter and you should import them manually:

Import extensions with: `require(demeter/extension/<method>)`.
For example:

```javascript
const demeter = require("demeter");
require("demeter/extension/csv");
demeter.csv(options);
```
or
```javascript
const demeterCsv = require("demeter/extension/csv");
demeterCsv(options);
```
