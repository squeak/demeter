
module.exports = {

  display: {
    title: "CORE METHODS",
    commment: "Core methods that are always available when yo import demeter.",
    borderColor: "primary",
  },

  methods: [
    //
    //                              _

    {
      name: "demeter",
      matcher: false,
      display: {
        title: "demeter — (essential methods)",
      },
      path: "core/_",
      methodsAutofill: "all",
    },

    //
    //                              LS SYNC

    {
      name: "demeter.lsSync",
      path: "core/lsSync",
    },

    //                              ¬
    //
  ],

};
