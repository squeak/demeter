
module.exports = {

  display: {
    title: "EXTENSION METHODS",
    commment: 'Extensions, you must import them with <pre>require("demeter/extension/&ltextensionName&gt")</pre>.',
    borderColor: "secondary",
  },

  methods: [
    //
    //                              CSV

    {
      name: "demeter.csv",
      path: "extension/csv",
    },

    //                              ¬
    //
  ],

};
